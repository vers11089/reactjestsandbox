import React, { useEffect } from 'react';
import { Headline } from '../headline';
import { Randoms } from '../randomPerson';
import { Link } from 'react-router-dom';

export const Home = () => {
  const [data, setData] = useState();
  const skills = [
    {
      title: 'React'
    },
    {
      title: '.NET'
    },
    {
      title: 'Angular'
    },
    {
      title: 'JavaScript in general'
    },
    {
      title: 'Ruby on Rails'
    },
    {
      title: 'C# in general'
    },
    {
      title: 'Linux command line/Git'
    }
  ];
  useEffect(() => {
    setData(skills);
  });

  return (
    <div className="App">
      <section className="headline">
        <Link to="/certifications">View Certifications</Link>
        <Headline
          header="Random people that don't have my certifications!! LOL"
          desc="Click 'Next' for random people"
        />
        <Randoms />
      </section>
      <h3>Array.map() of some of my main skills:</h3>
      {data.map((item, index) => (
        <p>{item.title}</p>
      ))}
    </div>
  );
  //}
};
